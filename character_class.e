note
	description: "[
		This class represents a character class within the Dark Souls RPG character sheet manager,
		encapsulating essential attributes that define each class, including name, description, level,
		and proficiencies. Character classes are fundamental to defining a character's capabilities,
		skills, and role within the game. Each class comes with specific saving throw proficiencies,
		primary stats, skill proficiencies, starting equipment, and unique abilities. This setup allows
		for a diverse range of characters, each tailored to different play styles and strategies,
		facilitating a rich and varied gaming experience.
	]"
	author: "Darvine64"
	date: "April 29th, 18:00"
	revision: "0.2"
	license: "[
	    This file is part of <Eldritch Codex Keeper>, which is distributed under the terms of the
	    GNU General Public License v3.0 or later.

	    <An immersive tool for managing RPG characters, inventories, and adventures, designed for fans of dark fantasy role-playing games.>

	    Copyright (C) <2024>  <Darvine64>

		This program is free software: you can redistribute it and/or modify
	    it under the terms of the GNU General Public License as published by
	    the Free Software Foundation, either version 3 of the License, or
	    (at your option) any later version.

		This program is distributed in the hope that it will be useful,
	    but WITHOUT ANY WARRANTY; without even the implied warranty of
	    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	    GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
	    along with this program. If not, see <http://www.gnu.org/licenses/>.

	 ]"


class
	CHARACTER_CLASS

create
    make

feature {NONE} -- Initialization

    make (a_name, a_description: STRING; a_level: INTEGER;
          a_saving_throw_proficiencies, a_primary_stats, a_skill_proficiencies, a_starting_equipments: LIST[STRING];
		  a_abilities: LIST[ABILITY])
        -- Initialize a character class with its name, description, level, saving throw proficiencies,
        -- primary stats, skill proficiencies, starting equipment, and abilities.
        do
			name := a_name
			description := a_description
			level := a_level
			saving_throw_proficiencies := a_saving_throw_proficiencies
			primary_stats := a_primary_stats
			skill_proficiencies := a_skill_proficiencies
			starting_equipments := a_starting_equipments
			abilities := a_abilities
        end

feature -- Attributes

    name, description: STRING
        -- The name and description of the character class.
    level: INTEGER
        -- The level of the character within this class.
    saving_throw_proficiencies, primary_stats, skill_proficiencies, starting_equipments: LIST[STRING]
        -- Lists of stats for saving throws and primary character attributes.
        -- Skills in which the character class has proficiency.
        -- Equipment items that characters of this class start with.
    abilities: LIST[ABILITY]
        -- Special abilities granted to characters of this class.

end
