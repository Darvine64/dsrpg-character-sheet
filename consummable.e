note
	description: "[
		This class represents a consumable item within the Dark Souls RPG character sheet manager, 
		extending the EQUIPMENT class to include consumables-specific attributes and behaviors. 
		Consumable items are those that can be used by characters to gain temporary benefits or effects 
		such as healing, buffs, or temporary enhancements. This class facilitates the creation, 
		management, and utilization of such items, ensuring they are properly integrated within 
		the game's mechanics and character interactions. The make feature initializes a consumable 
		with a name, cost, and effect, leveraging the EQUIPMENT class's structure for consistent 
		management across different types of equipment.
	]"
	author: "Darvine64"
	date: "April 29th, 18:00"
	revision: "0.2"
	license: "[
	    This file is part of <Eldritch Codex Keeper>, which is distributed under the terms of the
	    GNU General Public License v3.0 or later.

	    <An immersive tool for managing RPG characters, inventories, and adventures, designed for fans of dark fantasy role-playing games.>

	    Copyright (C) <2024>  <Darvine64>

		This program is free software: you can redistribute it and/or modify
	    it under the terms of the GNU General Public License as published by
	    the Free Software Foundation, either version 3 of the License, or
	    (at your option) any later version.

		This program is distributed in the hope that it will be useful,
	    but WITHOUT ANY WARRANTY; without even the implied warranty of
	    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	    GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
	    along with this program. If not, see <http://www.gnu.org/licenses/>.

	 ]"


class
	CONSUMMABLE

inherit
	EQUIPMENT
		redefine
			make
		end

create
	make

feature {NONE} -- Initialization

	make (a_name: STRING; a_cost: INTEGER; a_effect: STRING)
		-- Initialize a consumable item with its name, cost, and effect.
		-- `a_name`: The name of the consumable.
		-- `a_cost`: The cost of the consumable, indicating how much it costs to acquire in-game.
		-- `a_effect`: A description of the consumable's effect or utility when used.
		do
			Precursor(a_name, a_cost, a_effect) -- Calls the constructor of the parent EQUIPMENT class.
		end

end
