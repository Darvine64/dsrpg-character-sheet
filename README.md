# Eldritch Codex Keeper
## Version 0.1


## Description
The Eldritch Codex Keeper is a comprehensive tool designed to enhance the tabletop gaming experience for fans of the Dark Souls universe. This application allows players to create, manage, and save their character sheets digitally, integrating the deep lore and complex mechanics of Dark Souls into a user-friendly interface. From equipment and abilities to stats and spells, the Character Sheet Manager ensures that all aspects of your character are meticulously tracked and easily accessible.

## Visuals
Design is yet to be finished.

## Installation
- Setup Eiffel local variables and that the PATH for Eiffel is well configured.
	`export LANG=C
	export ISE_EIFFEL=/usr/local/Eiffel_22.05
	export ISE_PLATFORM=linux-x86-64
	export PATH=$PATH:$ISE_EIFFEL/studio/spec/$ISE_PLATFORM/bin
	`
- Compile the app: ec -config build_project.ecf -finalize -c_compile

- Select the target number [1] vision 2project -- EiffelBuild Generated Configuration for Project vision2_project.

- Enter `y` and ENTER


## Usage
Will be added later when there will be some use case.

## Support
You can join my Discord server! It is still a bit empty, but I as the developmet goes on the server will become more elaborate.

Here is the link to my Discord server: https://discord.gg/MqKFKDtw 

## Roadmap
Will be added later

## Contributing
Contributions to the Eldritch Codex Keeper are welcome! If you have ideas for features, bug fixes, or improvements, please fork the repository and submit a pull request.

Note: If you want to contribute and you've seeded data from an xml file. It's important that your file is not commited otherwise I'll have to decline your pull request.
## Authors and acknowledgment
Thanks to Marchalo for letting me use his Eiffel programming standard. See his work -> https://github.com/tioui

Special thanks to the Dark Souls community for the inspiration and support in developing this application.

## License
This project is under the GNU General Public License v3.0 

See the COPYING file to know more.

## Project status
In development...


## Seeding data (Important!)

If you want to use the application, you need to seed data from the books into an xml document.

Here are the steps to succefuly seed the data:

1. Move the template.xml file to /xml

2. Rename template.xml to data.xml

3. Open data.xml with your favorite xml editor

4. Fill out the file with the data that are from the book or enter your homebrew content

I tried to make the fields the most straightforward possible, but in case you're not sure about a certain field. Here is a glossary of the different fields:

### Glossary

If you want to add more than one item, you need to copy and paste the tag of the item that is not in the plural form and include all the tags within it.

**Warning**: DO NOT SEND ANY COPY OF YOUR FILE IF IT CONTAINS DATA FROM THE OFFICIAL STEAMFORGED GAMES RULEBOOK.

#### Consumables
- **Name**: The name of the consumable, like "Apple" or "Arrows".
- **Cost**: The cost in souls of the consumable.
  - This field supports only numbers, no letters.
  - If an item has a special cost, for example, "Cost 5 Souls per 10 'X'", you can write 5 and add a clarification in the effect tag.
  - If the item's cost value is "-", you can write "0" for the cost value.
- **Effect**: The effect of the consumable. For faster results, copy and paste is suggested.

#### Armors
- **Name**: The name of the armor.
- **Effect**: The special effect of the armor, if applicable; otherwise, enter "No special effect".
- **Description**: The lore-based or visual description of the item, not its explicit function.
- **Cost**: The cost in souls of the armor.
  - This field supports only numbers, no letters.
  - For items with special costs, follow the same instructions as for consumables.
- **Armor Class**: The base armor class provided by the armor.
- **Armor Class Stat Modifier**: The stat that modifies the base armor class, if any. Leave blank if none.
- **Strength Requirement**: The minimum strength needed to wear the armor, or "0" if there is no requirement.

#### Shields
- **Name**: The name of the shield.
- **Effect**: The special effect of the shield, if applicable; otherwise, enter "No special effect".
- **Description**: The lore-based or visual description of the shield.
- **Type**: The category of the shield, e.g., "Small Shield" or "Heavy Shield".
- **Cost**: The cost in souls of the shield, with the same rules as for armors.
- **Armor Class**: The base armor class bonus provided by the shield.
- **Strength Requirement**: The minimum strength needed to use the shield, or "0" if there is no requirement.

#### Weapons
- **Name**: The name of the weapon.
- **Effect**: The special effect of the weapon, if applicable; otherwise, enter "No special effect".
- **Type**: The category of the weapon, e.g., "Dagger" or "Longsword".
- **Damage Type**: The type of damage the weapon deals, e.g., "Piercing", "Slashing", "Bludgeoning".
- **Cost**: The cost in souls of the weapon, with the same rules as for armors.
- **Strength Requirement**: The minimum strength needed to use the weapon, or "0" if there is no requirement.
- **Number of Dice**: The number of damage dice used to calculate damage from the weapon.
- **Dice Max Value**: The maximum value of the dice used for calculating damage. For versatile weapons or those using different types of dice, add another "dice_max_value" tag within the "damage_dices" tag.
- **Property**: The various properties of the weapon, e.g., "Finesse", "Reload", "Light". For multiple properties, add another "property" tag within the "properties" tag.

#### Rings
- **Name**: The name of the ring.
- **Cost**: The cost in souls of the ring, following the same rules as for consumables.
- **Effect**: The special effect of the ring, if applicable; otherwise, enter "No special effect".

#### Spells
- **Name**: The name of the spell.
- **Effect**: A description of what the spell does.
- **Cast Time Type**: The type of action required to cast the spell, e.g., "Action", "Reaction", "Bonus Action".
- **Duration**: The duration that the spell stays in effect.
- **Range**: The range at which the spell can be cast.
- **Cost**: The resources consumed by the spell, if any; otherwise, enter "-" if there is no cost.
- **Spell Type**: The category of the spell, e.g., "Sorcery", "Pyromancy".
- **Cast Time Number**: The number of actions required to cast the spell. Enter "1" by default, or more if needed.
- **Casts**: The number of times the spell can be cast before requiring a rest.
- **Level**: The required level to cast the spell.
- **Attunement Slots**: The number of attunement slots the spell occupies.

#### Origins
- **Name**: The name of the origin.
- **Description**: Key information or tips about this origin.
- **Bloodied Effect**: The effect triggered when becoming bloodied. For multiple effects, add another "bloodied_effect" tag within the "bloodied_effects" tag.
- **Speed**: The base speed of the origin, supporting only numbers.
- **Base Stat**: The base stats for character creation, in the order of "Strength", "Dexterity", "Constitution", "Intelligence", "Wisdom", and "Charisma".
- **Position Dice**: The type of die used to calculate position.

#### Abilities
- **Name**: The name of the ability.
- **Unique Name**: A unique identifier for the ability, recommended to combine the ability name with its class, e.g., "Extra Attack Knight".
- **Description**: A description of what the ability does.
- **Usage**: The number of times the ability can be used.
- **Option**: For abilities requiring a choice, detail the options available.

#### Classes
- **Name**: The name of the class.
- **Description**: Key information or tips about this class.
- **Level**: The starting level for the class, with "0" recommended unless specific conditions apply.
- **Stat**: The saving throw stats with proficiency for the class. For multiple proficiencies, add another "stat" tag within.
- **Primary Stat**: The primary stat used by the class. For multiple primary stats, add another "primary_stat" tag within.
- **Skill Proficiency**: The skills in which the class can gain proficiency. For multiple proficiencies, add another "skill_proficiency" tag within.
- **Starting Equipment Name**: The name of the starting equipment for the class. For multiple items, add another "starting_equipment_name" tag within.
- **Ability Unique Name**: The unique name of an ability associated with the class. For multiple abilities, add another "ability_unique_name" tag within.





