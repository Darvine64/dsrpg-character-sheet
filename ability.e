note
	description: "[
		The ABILITY class represents a character's ability within the Dark Souls RPG character sheet 
		manager. Abilities are key components of character customization and development, offering 
		unique actions, skills, or spells that a character can utilize. Each ability is defined by 
		its name, unique identifier, description, usage parameters (such as cooldown or charges), 
		required level for acquisition, and any options or variations the ability may have. This 
		class enables the detailed tracking and application of abilities, enhancing the gameplay 
		experience by allowing for diverse and strategic character builds.
	]"
	author: "Darvine64"
	date: "February 29th, 22:00"
	revision: "0.2"
	license: "[
	    This file is part of <Eldritch Codex Keeper>, which is distributed under the terms of the
	    GNU General Public License v3.0 or later.

	    <An immersive tool for managing RPG characters, inventories, and adventures, designed for fans of dark fantasy role-playing games.>

	    Copyright (C) <2024>  <Darvine64>

		This program is free software: you can redistribute it and/or modify
	    it under the terms of the GNU General Public License as published by
	    the Free Software Foundation, either version 3 of the License, or
	    (at your option) any later version.

		This program is distributed in the hope that it will be useful,
	    but WITHOUT ANY WARRANTY; without even the implied warranty of
	    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	    GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
	    along with this program. If not, see <http://www.gnu.org/licenses/>.

	 ]"


class
	ABILITY

create
	make

feature {NONE} -- Initialization

	make (a_name, a_unique_name, a_description: STRING; a_usage, a_required_level: INTEGER;
	      a_options: LIST[STRING])
		-- Initialize an ability with its name, unique identifier, description, usage, required
		-- level, and options.
		do
			name := a_name
			unique_name := a_unique_name
			description := a_description
			usage := a_usage
			required_level := a_required_level
			options := a_options
		end

feature -- Attributes

	name, unique_name, description: STRING
		-- Name, unique identifier, and description of the ability.
	usage, required_level: INTEGER
		-- Usage parameters and level required to acquire the ability.
	options: LIST[STRING]
		-- Additional options or variations available for the ability.

end
