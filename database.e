note
	description: "[
		This class manages interactions with an SQLite database for the Dark Souls RPG character
		sheet manager application. It handles initializing the database, creating the schema if 
		necessary, checking the validity of the database, and seeding initial data. Furthermore,
		it provides functionalities to retrieve character data and potentially other game-related 
		data from the database, ensuring that user progress and character details are maintained 
		across sessions. The class is designed to work in conjunction with data loaded from XML 
		to populate the game with copyright-sensitive information, enabling a seamless user 
		experience by loading saved characters and merging them with game data.
	]"
	author: "Darvine64"
	date: "April 29th, 18:00"
	revision: "0.2"
	license: "[
	    This file is part of <Eldritch Codex Keeper>, which is distributed under the terms of the
	    GNU General Public License v3.0 or later.

	    <An immersive tool for managing RPG characters, inventories, and adventures, designed for fans of dark fantasy role-playing games.>

	    Copyright (C) <2024>  <Darvine64>

		This program is free software: you can redistribute it and/or modify
	    it under the terms of the GNU General Public License as published by
	    the Free Software Foundation, either version 3 of the License, or
	    (at your option) any later version.

		This program is distributed in the hope that it will be useful,
	    but WITHOUT ANY WARRANTY; without even the implied warranty of
	    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	    GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
	    along with this program. If not, see <http://www.gnu.org/licenses/>.

	 ]"


class
	DATABASE_MANAGER

create
	make

feature {NONE} -- Initialization

	make
		-- Initializes the database connection.
		do
			create sqlite_database.make_create_read_write (sql_file_source.file_path.name)
		end

feature -- Database operations

	initialize
		-- Initialize the database by validating its structure and seeding initial data if necessary.
		do
			if not database_valid then
				create_database_schema
				seed_data
			end
		end

	database_valid: BOOLEAN
		-- Check if the database and its schema are valid and existing.
		local
			l_query: SQLITE_QUERY_STATEMENT
			l_error: BOOLEAN

		do
			l_error := not database_exists
			create l_query.make ("SELECT * FROM characters;", sqlite_database)
			l_error := l_error or not l_query.is_compiled
			create l_query.make ("SELECT * FROM class;", sqlite_database)
			l_error := l_error or not l_query.is_compiled
			create l_query.make ("SELECT * FROM features;", sqlite_database)
			l_error := l_error or not l_query.is_compiled
			create l_query.make ("SELECT * FROM stats;", sqlite_database)
			l_error := l_error or not l_query.is_compiled
			create l_query.make ("SELECT * FROM equipments;", sqlite_database)
			l_error := l_error or not l_query.is_compiled
			create l_query.make ("SELECT * FROM spells;", sqlite_database)
			l_error := l_error or not l_query.is_compiled
			Result := not l_error
		end

	database_exists: BOOLEAN
			-- Check if the database file exists.
		do
			Result := sql_file_source.exists
		end

	create_database_schema
			-- Create the database schema based on predefined SQL statements.
		local
			l_modify: SQLITE_MODIFY_STATEMENT
		do
			sqlite_database.begin_transaction (False)

			across
				["CREATE TABLE IF NOT EXISTS characters (CharacterId INTEGER PRIMARY KEY, Name TEXT, Origin TEXT, Speed INTEGER, Souls INTEGER, SoulsSpent INTEGER, CurrentPosition INTEGER, Backstory TEXT, Memory TEXT, Drive TEXT, Note TEXT);",
				"CREATE TABLE IF NOT EXISTS class (ClassId INTEGER PRIMARY KEY, Name TEXT, CharacterId INTEGER, FOREIGN KEY (CharacterId) REFERENCES characters(CharacterId));",
				"CREATE TABLE IF NOT EXISTS features (FeatureId INTEGER PRIMARY KEY, Name TEXT, Usage INTEGER, CharacterId INTEGER, FOREIGN KEY (CharacterId) REFERENCES characters(CharacterId));",
				"CREATE TABLE IF NOT EXISTS stats (StatId INTEGER PRIMARY KEY, Name TEXT, Value INTEGER, CharacterId INTEGER, FOREIGN KEY (CharacterId) REFERENCES characters(CharacterId));",
				"CREATE TABLE IF NOT EXISTS equipments (EquipmentId INTEGER PRIMARY KEY, Name TEXT, Equipped BOOLEAN, Usage INTEGER, CharacterId INTEGER, FOREIGN KEY (CharacterId) REFERENCES characters(CharacterId));",
				"CREATE TABLE IF NOT EXISTS spells (SpellId INTEGER PRIMARY KEY, Name TEXT, Casts INTEGER, Attuned BOOLEAN, CharacterId INTEGER, FOREIGN KEY (CharacterId) REFERENCES characters(CharacterId));"] as cursor loop
				if attached cursor.item as l_item then
					create l_modify.make (l_item.out, sqlite_database)
					l_modify.execute
				end

			end
			sqlite_database.commit
		end

	seed_data
			-- Seed the database with initial or default data.
		local

			l_insert: SQLITE_INSERT_STATEMENT
		do

			sqlite_database.begin_transaction (False)
			create l_insert.make ("INSERT INTO characters (CharacterId, Name, Origin, Speed, Souls, SoulsSpent, CurrentPosition) VALUES ('1','Patrick', 'The Fencer', '25', '0', '0', '11');", sqlite_database)
			l_insert.execute
			sqlite_database.commit
		end

--	get_all_avatars: LIST[CHARACTER]

--	    -- Retrieve all avatars from the database and return them as a list of AVATAR objects.
--	    local
--	    	l_statement: SQLITE_STATEMENT
--	    	l_result_row: SQLITE_RESULT_ROW
--	    	l_characters: LIST[AVATAR]
--	    	l_character: AVATAR
--	    	l_class: CHARACTER_CLASS
--	    	l_equipments: LIST[EQUIPMENT]
--	    	l_origin: ORIGIN
--	    	l_skills: LIST[SKILL]
--	    	l_stats: LIST[STAT]
--	    	l_spells: LIST[SPELL]
--		do
--			create l_statement.make ("SELECT * FROM characters", Current)
--			across
--				l_statement.arguments_count as element
--			loop
--				l_result_row := l_statement.execute_new.item

--				if not l_result_row.is_null (1) then
--					l_class:= make_class(l_result_row.value (1))
--				end
--			end

--		end

	get_character (a_id: INTEGER): SQLITE_QUERY_STATEMENT
		-- Retrieve character data for the given character ID.
		local
			l_query: SQLITE_QUERY_STATEMENT

		do
			create l_query.make ("SELECT * FROM characters WHERE CharacterId == {a_id};", sqlite_database)
			Result := l_query
		end

feature {NONE} -- Creating objects

--	make_class(a_character_id: INTEGER): CHARACTER_CLASS

--		local
--	    	l_statement: SQLITE_STATEMENT
--	    	l_result_row: SQLITE_RESULT_ROW
--	    do
--	    	create l_statement.make ("SELECT * FROM characters", sqlite_database)
--	    end

feature -- Implementation

	sql_file_source: SQLITE_FILE_SOURCE
			-- Source of the SQLite database file.
		do
			create Result.make ("db/DSRPG_DATABASE.db")
		end

	sqlite_database: SQLITE_DATABASE
		-- Represents the SQLite database connection.
end
