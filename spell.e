note
	description: "[
		This class defines a spell within the Dark Souls RPG character sheet manager,
	  	detailing the spell's name, effect, casting time, duration, range, level requirement,
	  	attunement slots, and whether it is attuned for use. Spells are crucial for gameplay,
	  	providing characters with magical abilities that can affect combat, healing, defense,
	  	and utility. This class enables the management and application of spells, enhancing
	  	the magical aspects of character development and interaction within the game.
	 ]"
	author: "Darvine64"
	date: "April 29th, 18:00"
	revision: "0.2"
	license: "[
	    This file is part of <Eldritch Codex Keeper>, which is distributed under the terms of the
	    GNU General Public License v3.0 or later.

	    <An immersive tool for managing RPG characters, inventories, and adventures, designed for fans of dark fantasy role-playing games.>

	    Copyright (C) <2024>  <Darvine64>

		This program is free software: you can redistribute it and/or modify
	    it under the terms of the GNU General Public License as published by
	    the Free Software Foundation, either version 3 of the License, or
	    (at your option) any later version.

		This program is distributed in the hope that it will be useful,
	    but WITHOUT ANY WARRANTY; without even the implied warranty of
	    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	    GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
	    along with this program. If not, see <http://www.gnu.org/licenses/>.

	 ]"

class
	SPELL

create
	make

feature {NONE}

	make (a_name, a_effect, a_cast_time_type, a_duration, a_range, a_cost, a_spell_type: STRING;
		  a_cast_time_number, a_casts, a_level, a_attunement_slots: INTEGER; a_is_attuned: BOOLEAN)
			-- Initializes a spell with comprehensive details, allowing for in-depth
			-- management and usage within the game. Spells enhance gameplay with magical
			-- abilities and effects.
			-- `a_name`: Spell name.
			-- `a_effect`: Description of spell effect.
			-- `a_cast_time_type`: Type of casting time (action, reaction, bonus action).
			-- `a_cast_time_number`: Number of actions required to cast.
			-- `a_casts`: Uses per long rest.
			-- `a_duration`: Spell duration.
			-- `a_range`: Casting range.
			-- `a_level`: Required level to cast.
			-- `a_attunement_slots`: Attunement slots required.
			-- `a_cost`: Casting cost.
			-- `a_is_attuned`: Whether the spell is attuned for use.
			-- `a_spell_type`: The type of spell (e.g., offensive, defensive, utility).
		do
			name := a_name
			effect := a_effect
			cast_time_type := a_cast_time_type
			cast_time_number := a_cast_time_number
			casts := a_casts
			duration := a_duration
			range := a_range
			level := a_level
			attunement_slots := a_attunement_slots
			cost := a_cost
			is_attuned := a_is_attuned
			spell_type := a_spell_type
		end

feature -- Attributes

	name, effect, cast_time_type, duration, range, cost, spell_type: STRING
	cast_time_number, casts, level, attunement_slots: INTEGER
	is_attuned: BOOLEAN

end
