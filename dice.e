note
	description: "[
		This class represents a dice in the Dark Souls RPG character sheet manager, 
		enabling users to simulate dice rolls within the application. It inherits from 
		`DICE_RANDOM_SHARED` to utilize shared random generation functionalities, 
		providing a method to roll a dice of any specified maximum value. This feature 
		allows for the dynamic generation of random outcomes in various game scenarios, 
		mirroring the unpredictability and chance found in tabletop RPGs and other dice-based 
		games. The `roll_dice` feature returns a random integer between 1 and the `max_value`, 
		representing the result of a dice roll.
	]"
	author: "Darvine64"
	date: "April 29th, 18:00"
	revision: "0.2"
	license: "[
	    This file is part of <Eldritch Codex Keeper>, which is distributed under the terms of the
	    GNU General Public License v3.0 or later.

	    <An immersive tool for managing RPG characters, inventories, and adventures, designed for fans of dark fantasy role-playing games.>

	    Copyright (C) <2024>  <Darvine64>

		This program is free software: you can redistribute it and/or modify
	    it under the terms of the GNU General Public License as published by
	    the Free Software Foundation, either version 3 of the License, or
	    (at your option) any later version.

		This program is distributed in the hope that it will be useful,
	    but WITHOUT ANY WARRANTY; without even the implied warranty of
	    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	    GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
	    along with this program. If not, see <http://www.gnu.org/licenses/>.

	 ]"


class
	DICE

inherit
	DICE_RANDOM_SHARED

create
	make

feature {NONE} -- Initialization

	make (a_max_value: INTEGER)
		-- Initialize the dice with a maximum value `a_max_value`.
		-- `a_max_value`: The highest number that can be rolled on this dice.
		do
			max_value := a_max_value
		end

feature -- Rolling the dice

	roll_dice: INTEGER
		-- Roll the dice and return a random number between 1 and `max_value`.
		-- This simulates the act of rolling a physical dice.
		do
			random.generate_new_random
			Result := random.last_random_integer_between (1, max_value)
		end

feature -- Attributes

	max_value: INTEGER
		-- The maximum value that can be rolled on this dice.

end
