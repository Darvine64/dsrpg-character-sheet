note
	description: "[
		This class represents armor within the Dark Souls RPG character sheet manager, extending
		the EQUIPMENT class to include armor-specific attributes. Armor is crucial for character
		defense, providing not just a damage mitigation value (armor class) but also potentially
		enhancing or modifying a character's stats. Each armor piece includes a name, effect,
		description, cost, armor class value, strength requirement, and an armor class stat modifier.
		This setup allows for a diverse array of armor options, each with unique effects and
		requirements, contributing to the strategic depth of character customization and gameplay.
	]"
	author: "Darvine64"
	date: "April 29th, 18:00"
	revision: "0.2"
	license: "[
	    This file is part of <Eldritch Codex Keeper>, which is distributed under the terms of the
	    GNU General Public License v3.0 or later.

	    <An immersive tool for managing RPG characters, inventories, and adventures, designed for fans of dark fantasy role-playing games.>

	    Copyright (C) <2024>  <Darvine64>

		This program is free software: you can redistribute it and/or modify
	    it under the terms of the GNU General Public License as published by
	    the Free Software Foundation, either version 3 of the License, or
	    (at your option) any later version.

		This program is distributed in the hope that it will be useful,
	    but WITHOUT ANY WARRANTY; without even the implied warranty of
	    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	    GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
	    along with this program. If not, see <http://www.gnu.org/licenses/>.

	 ]"


class
	ARMOR

inherit
	EQUIPMENT
		rename
			make as make_equipment
		end

create
	make

feature {NONE} -- Initialization

	make (a_name, a_effect, a_description, a_armor_class_stat_modifier: STRING;
	      a_cost, a_armor_class, a_strength_requirement: INTEGER)
		-- Initialize armor with its specific attributes.
		-- `a_name`: The name of the armor.
		-- `a_effect`: The effect or utility provided by the armor.
		-- `a_description`: A detailed description of the armor.
		-- `a_armor_class_stat_modifier`: Modifier affecting the armor class from stats.
		-- `a_cost`: The cost of the armor, representing how much it costs to acquire in-game.
		-- `a_armor_class`: The armor class provided by this piece, contributing to defense.
		-- `a_strength_requirement`: Minimum strength required to use the armor effectively.
		do
			name := a_name
			cost := a_cost
			effect := a_effect
			description := a_description
			armor_class := a_armor_class
			strength_requirement := a_strength_requirement
			armor_class_stat_modifier := a_armor_class_stat_modifier
		end

feature -- Attributes

	description, armor_class_stat_modifier: STRING
		-- Description of the armor and the stat modifier that affects armor class.
	armor_class, strength_requirement: INTEGER
		-- The armor class value and strength requirement for using the armor.

end
