note
	description: "[
		This class represents a single statistical attribute (or 'stat') for a character in the Dark Souls RPG character sheet manager,
		such as Strength (STR), Dexterity (DEX), Constitution (CON), Intelligence (INT), Wisdom (WIS), or Charisma (CHA). It encapsulates
		the stat's name, its numerical value, and the derived modifier, which is used to calculate the effects of the stat on gameplay,
		such as influencing skill checks, combat efficiency, and other character actions. The modifier is computed based on the stat value,
		following standard tabletop RPG conventions, where it affects a wide range of in-game mechanics.
	]"
	author: "Darvine64"
	date: "April 29th, 18:00"
	revision: "0.2"
	license: "[
	    This file is part of <Eldritch Codex Keeper>, which is distributed under the terms of the
	    GNU General Public License v3.0 or later.

	    <An immersive tool for managing RPG characters, inventories, and adventures, designed for fans of dark fantasy role-playing games.>

	    Copyright (C) <2024>  <Darvine64>

		This program is free software: you can redistribute it and/or modify
	    it under the terms of the GNU General Public License as published by
	    the Free Software Foundation, either version 3 of the License, or
	    (at your option) any later version.

		This program is distributed in the hope that it will be useful,
	    but WITHOUT ANY WARRANTY; without even the implied warranty of
	    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	    GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
	    along with this program. If not, see <http://www.gnu.org/licenses/>.

	 ]"

class
	STAT

create
	make

feature {NONE}

	make (a_name: STRING; a_value: INTEGER)
		-- Initialize a stat with a name and its value, automatically calculating its modifier.
		-- `a_name`: The name of the stat, e.g., "STR", "DEX", "CON", "INT", "WIS", "CHA".
		-- `a_value`: The numerical value of the stat, typically ranging from 1 to 30.
		do
			name := a_name
			value := a_value
			modifier := (value - 10) // 2 -- Modifier calculated as per RPG standards.
		end

feature -- Access

	name: STRING
		-- The name of the stat.

	value, modifier: INTEGER
		-- `value`: Numerical value of the stat.
		-- `modifier`: Derived bonus or penalty from the stat value, impacting various game aspects.

end
