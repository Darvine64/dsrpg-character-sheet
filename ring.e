note
	description: "[
		This class represents a ring in the Dark Souls RPG character sheet manager,
  		extending EQUIPMENT to include ring-specific attributes and functionalities. Rings
  		offer various effects, from stat boosts to unique abilities. This class facilitates
  		ring item creation and management, encapsulating name, cost, and effects. It's designed
 		to integrate seamlessly with the equipment management system, making rings easily
  		added, tracked, and utilized within the game's mechanics.
	 ]"
	author: "Darvine64"
	date: "April 29th, 18:00"
	revision: "0.2"
	license: "[
	    This file is part of <Eldritch Codex Keeper>, which is distributed under the terms of the
	    GNU General Public License v3.0 or later.

	    <An immersive tool for managing RPG characters, inventories, and adventures, designed for fans of dark fantasy role-playing games.>

	    Copyright (C) <2024>  <Darvine64>

		This program is free software: you can redistribute it and/or modify
	    it under the terms of the GNU General Public License as published by
	    the Free Software Foundation, either version 3 of the License, or
	    (at your option) any later version.

		This program is distributed in the hope that it will be useful,
	    but WITHOUT ANY WARRANTY; without even the implied warranty of
	    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	    GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
	    along with this program. If not, see <http://www.gnu.org/licenses/>.

	 ]"


class
	RING

inherit
	EQUIPMENT
		redefine
			make
		end

create
	make

feature {NONE} -- Initialization

	make (a_name: STRING; a_cost: INTEGER; a_effect: STRING)
			-- Initialize a ring with its name, cost, and effect. This constructor
			-- calls the EQUIPMENT class constructor with the provided values, ensuring
			-- the ring is set up as equipment in the game.
			-- `a_name`: The ring's name.
			-- `a_cost`: Acquisition cost in-game.
			-- `a_effect`: The ring's effect or utility when worn.
		do
			Precursor(a_name, a_cost, a_effect) -- Calls EQUIPMENT constructor.
		end

end
