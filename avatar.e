note
	description: "[
		This class serves as the central character model within the Dark Souls RPG character sheet 
		manager, encapsulating all relevant character information. Though named 'AVATAR' due to naming 
		constraints, it embodies the concept of a character, including their name, memory, drive, 
		personal notes, and various game mechanics such as proficiency bonus, soul count, equipment, 
		and abilities. Characters are defined not just by their statistical attributes but also by 
		their backstory elements like memory and drive, providing a deep role-playing experience. 
		The class ties together various aspects of a character's identity, from their origins and 
		class to their skills, stats, and spells, facilitating a comprehensive and immersive 
		characterization process.
	]"
	author: "Darvine64"
	date: "February 29th, 22:00"
	revision: "0.1"
	license: "[
	    This file is part of <Eldritch Codex Keeper>, which is distributed under the terms of the
	    GNU General Public License v3.0 or later.

	    <An immersive tool for managing RPG characters, inventories, and adventures, designed for fans of dark fantasy role-playing games.>

	    Copyright (C) <2024>  <Darvine64>

		This program is free software: you can redistribute it and/or modify
	    it under the terms of the GNU General Public License as published by
	    the Free Software Foundation, either version 3 of the License, or
	    (at your option) any later version.

		This program is distributed in the hope that it will be useful,
	    but WITHOUT ANY WARRANTY; without even the implied warranty of
	    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	    GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
	    along with this program. If not, see <http://www.gnu.org/licenses/>.

	 ]"

	 
class
	AVATAR

create
    make

feature {NONE} -- Initialization

    make (a_name, a_memory, a_drive, a_personal_note: STRING;
    	  a_proficiency_bonus, a_souls_count, a_souls_spent, a_current_position: INTEGER;
    	  a_character_class: CHARACTER_CLASS; a_equipments: LIST[EQUIPMENT]; a_origin: ORIGIN;
    	  a_skills: LIST[SKILL]; a_stats: LIST[STAT]; a_spells: LIST[SPELL])
        -- Initialize an avatar with all character-related details.
        do
			name := a_name
			memory := a_memory
			drive := a_drive
			personal_note := a_personal_note
			proficiency_bonus := a_proficiency_bonus
			souls_count := a_souls_count
			souls_spent := a_souls_spent
			current_position := a_current_position
			character_class := a_character_class
			equipments := a_equipments
			origin := a_origin
			skills := a_skills
			stats := a_stats
			spells := a_spells
        end

feature -- Attributes

    name, memory, drive, personal_note: STRING
        -- Character's name, memory, drive, and personal notes for backstory and role-playing.
    proficiency_bonus, souls_count, souls_spent, current_position: INTEGER
        -- Gameplay mechanics: proficiency bonus, soul currency, and character's current position.
    character_class: CHARACTER_CLASS
        -- The character's class, defining abilities, and class-specific traits.
    equipments: LIST[EQUIPMENT]
        -- List of equipment items the character possesses.
    origin: ORIGIN
        -- The character's origin, providing unique background and attributes.
    skills: LIST[SKILL]
        -- Skills the character is proficient in.
    stats: LIST[STAT]
        -- Character's statistical attributes.
    spells: LIST[SPELL]
        -- Spells available to the character.

end
