note
	description: "[
		This class represents a skill within the Dark Souls RPG character sheet manager,
	  	encapsulating the mechanics related to skill proficiency, expertise, and the calculation
	  	of skill modifiers. Skills are abilities characters can be proficient or expert in,
	  	influenced by their stats. This class allows for the creation and management of such
	  	skills, detailing their name, whether the character has proficiency or expertise in
	  	them, the stat they are associated with, and the character's proficiency bonus to
	 	accurately calculate the skill's modifier.
	 ]"
	author: "Darvine64"
	date: "April 29th, 18:00"
	revision: "0.2"
	license: "[
	    This file is part of <Eldritch Codex Keeper>, which is distributed under the terms of the
	    GNU General Public License v3.0 or later.

	    <An immersive tool for managing RPG characters, inventories, and adventures, designed for fans of dark fantasy role-playing games.>

	    Copyright (C) <2024>  <Darvine64>

		This program is free software: you can redistribute it and/or modify
	    it under the terms of the GNU General Public License as published by
	    the Free Software Foundation, either version 3 of the License, or
	    (at your option) any later version.

		This program is distributed in the hope that it will be useful,
	    but WITHOUT ANY WARRANTY; without even the implied warranty of
	    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	    GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
	    along with this program. If not, see <http://www.gnu.org/licenses/>.

	 ]"

class
    SKILL

create
    make

feature {NONE} -- Initialization

    make (a_name: STRING; a_is_proficiency, a_is_expertise: BOOLEAN; a_associated_stat: STAT; a_proficiency_bonus: INTEGER)
            -- Initialize a skill with its name, proficiency status, expertise status,
            -- associated stat, and proficiency bonus for accurate modifier calculation.
            -- `a_name`: Name of the skill.
            -- `a_is_proficiency`: Indicates proficiency in this skill.
            -- `a_is_expertise`: Indicates expertise in this skill, enhancing proficiency bonus.
            -- `a_associated_stat`: Stat the skill is based on, affecting base modifier.
            -- `a_proficiency_bonus`: Proficiency bonus applied to the skill modifier.
        local
            base_modifier: INTEGER
        do
            name := a_name
            is_proficiency := a_is_proficiency
            is_expertise := a_is_expertise
            base_modifier := (a_associated_stat.value - 10) // 2
            modifier := calculate_modifier (base_modifier, a_proficiency_bonus)
        end

    calculate_modifier (a_base_modifier, a_proficiency_bonus: INTEGER): INTEGER
            -- Calculate skill modifier based on base modifier and proficiency bonus.
            -- Applies double proficiency bonus for expertise.
        do
            if is_proficiency then
                Result := a_base_modifier + a_proficiency_bonus
            elseif is_expertise then
                Result := a_base_modifier + 2 * a_proficiency_bonus
            else
                Result := a_base_modifier
            end
        end

feature -- Attributes

    name: STRING
            -- Skill name.
    modifier: INTEGER
            -- Calculated skill modifier, combining base stat modifier with proficiency bonus.
    is_proficiency, is_expertise: BOOLEAN
            -- Flags for proficiency and expertise in the skill.

end
