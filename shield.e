note
	description: "[
		This class represents a shield in the Dark Souls RPG character sheet manager,
	  		extending the EQUIPMENT class. Shields provide defense boosts, requiring specific
	  		strength to wield and offering various effects. This class allows defining shields with
	  		their unique attributes such as armor class and strength requirements, alongside the
	  		standard equipment attributes like name, cost, and effect.
	 ]"
	author: "Darvine64"
	date: "April 29th, 18:00"
	revision: "0.2"
	license: "[
	    This file is part of <Eldritch Codex Keeper>, which is distributed under the terms of the
	    GNU General Public License v3.0 or later.

	    <An immersive tool for managing RPG characters, inventories, and adventures, designed for fans of dark fantasy role-playing games.>

	    Copyright (C) <2024>  <Darvine64>

		This program is free software: you can redistribute it and/or modify
	    it under the terms of the GNU General Public License as published by
	    the Free Software Foundation, either version 3 of the License, or
	    (at your option) any later version.

		This program is distributed in the hope that it will be useful,
	    but WITHOUT ANY WARRANTY; without even the implied warranty of
	    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	    GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
	    along with this program. If not, see <http://www.gnu.org/licenses/>.

	 ]"


class
	SHIELD

inherit
	EQUIPMENT
		rename
			make as make_equipment
		end

create
	make

feature {NONE} -- Initialization

	make (a_name, a_effect, a_description, a_type: STRING;
		  a_cost, a_armor_class, a_strength_requirement: INTEGER)
			-- Initialize a shield with its specific properties.
			-- `a_name`: Name of the shield.
			-- `a_cost`: Cost to acquire the shield in-game.
			-- `a_effect`: The effect or utility provided by the shield.
			-- `a_description`: Lore or background information.
			-- `a_type`: The shield's type (e.g., small shields, standard shields, Greatshield).
			-- `a_armor_class`: Defense boost provided by the shield.
			-- `a_strength_requirement`: Minimum strength to wield the shield.
		do
			name := a_name
			cost := a_cost
			effect := a_effect
			description := a_description
			armor_class := a_armor_class
			strength_requirement := a_strength_requirement
			type := a_type
		end

feature -- Attributes

	description, type: STRING
			-- Lore and type of the shield.
	armor_class, strength_requirement: INTEGER
			-- Defense boost and strength requirement to wield.

end
