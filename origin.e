note
	description: "[
		This class represents the origin of a character in the Dark Souls RPG character
	  	sheet manager, akin to race in other RPGs. Origins affect abilities, including base
	  	stats, speed, and special effects under certain conditions (e.g., 'bloodied effects').
	 	The 'position dice' is akin to D&D's Hit Dice, crucial for health/stamina and survival.
	  	This class encapsulates these attributes for character creation and management.
	 ]"
	author: "Darvine64"
	date: "April 29th, 18:00"
	revision: "0.2"
	license: "[
	    This file is part of <Eldritch Codex Keeper>, which is distributed under the terms of the
	    GNU General Public License v3.0 or later.

	    <An immersive tool for managing RPG characters, inventories, and adventures, designed for fans of dark fantasy role-playing games.>

	    Copyright (C) <2024>  <Darvine64>

		This program is free software: you can redistribute it and/or modify
	    it under the terms of the GNU General Public License as published by
	    the Free Software Foundation, either version 3 of the License, or
	    (at your option) any later version.

		This program is distributed in the hope that it will be useful,
	    but WITHOUT ANY WARRANTY; without even the implied warranty of
	    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	    GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
	    along with this program. If not, see <http://www.gnu.org/licenses/>.

	 ]"


class
	ORIGIN

create
	make

feature {NONE} -- Initialization

	make (a_name, a_description: STRING; a_bloodied_effects: LIST[STRING]; a_speed: INTEGER;
		  a_base_stats: LIST[STAT]; a_position_dice: DICE)
			-- Initialize origin with name, description, bloodied effects, speed, base stats,
			-- and position dice.
			-- `a_name`: Origin's name.
			-- `a_description`: Lore/background information.
			-- `a_bloodied_effects`: Buffs/abilities for when health is halved.
			-- `a_speed`: Base speed value, affects movement.
			-- `a_base_stats`: Starting stats/attributes for this origin.
			-- `a_position_dice`: Like D&D's Hit Dice, for health/stamina pool.
		do
			name := a_name
			description := a_description
			bloodied_effects := a_bloodied_effects
			speed := a_speed
			base_stats := a_base_stats
			position_dice := a_position_dice
		end

feature -- Attributes

	name, description: STRING
			-- Origin's name and description.

	bloodied_effects: LIST[STRING]
			-- Activates when health is at half, offers tactical advantages.

	speed: INTEGER
			-- Base speed for characters of this origin.

	base_stats: LIST[STAT]
			-- Foundation stats/attributes for characters.

	position_dice: DICE
			-- Like Hit Dice, critical for survival and combat.
end
