note
	description: "[
		This class defines a weapon within the Dark Souls RPG character sheet manager, 
		extending the EQUIPMENT class to include weapon-specific attributes such as type, 
		damage type, strength requirement, properties, and damage dices. Weapons are crucial 
		for combat, affecting how characters engage with enemies and the environment. The 
		`WEAPON` class allows for detailed weapon management, accommodating various weapon 
		types and their effects, damage calculations, and usage requirements within the game.
	]"
	author: "Darvine64"
	date: "April 29th, 18:00"
	revision: "0.2"
	license: "[
	    This file is part of <Eldritch Codex Keeper>, which is distributed under the terms of the
	    GNU General Public License v3.0 or later.
	    
	    <An immersive tool for managing RPG characters, inventories, and adventures, designed for fans of dark fantasy role-playing games.>
	     
	    Copyright (C) <2024>  <Darvine64>

		This program is free software: you can redistribute it and/or modify
	    it under the terms of the GNU General Public License as published by
	    the Free Software Foundation, either version 3 of the License, or
	    (at your option) any later version.

		This program is distributed in the hope that it will be useful,
	    but WITHOUT ANY WARRANTY; without even the implied warranty of
	    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	    GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
	    along with this program. If not, see <http://www.gnu.org/licenses/>.
	  ]"


class
	WEAPON

inherit
	EQUIPMENT
		rename
			make as make_equipment
		end

create
	make

feature {NONE} -- Initialization

	make (a_name, a_effect, a_type, a_damage_type: STRING; a_cost, a_strength_requirement,
	      a_number_dice: INTEGER; a_properties: LIST[STRING]; a_damage_dices: LIST[DICE])
		-- Initialize a weapon with its name, effect, type, damage type, cost, strength
		-- requirement, properties, damage dices, and number of dice.
		-- `a_name`: The name of the weapon.
		-- `a_effect`: The effect or utility provided by the weapon.
		-- `a_type`: The type of the weapon (e.g., melee, ranged).
		-- `a_damage_type`: The type of damage the weapon deals (e.g., slashing, piercing).
		-- `a_cost`: The cost of the weapon, in-game currency.
		-- `a_strength_requirement`: The strength requirement to effectively wield the weapon.
		-- `a_properties`: A list of properties or special features of the weapon.
		-- `a_damage_dices`: The dice used for calculating damage dealt by the weapon.
		-- `a_number_dice`: The number of dice to roll for weapon damage.
		do
			name := a_name
			cost := a_cost
			effect := a_effect
			type := a_type
			strength_requirement := a_strength_requirement
			properties := a_properties
			damage_type := a_damage_type
			damage_dices := a_damage_dices
			number_dice := a_number_dice
		end

feature -- Attributes

	type, damage_type: STRING -- The weapon's type and the type of damage it deals.
	strength_requirement: INTEGER -- Strength required to wield the weapon.
	properties: LIST[STRING] -- Properties or special features of the weapon.
	damage_dices: LIST[DICE] -- Dice for calculating damage.
	number_dice: INTEGER -- Number of dice to roll for damage.

end

