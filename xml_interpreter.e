note
	description: "[
			This class serves as an XML interpreter for the Dark Souls RPG character sheet manager,
			providing functionalities to parse XML documents that describe various game elements such
			as consumables, armors, shields, weapons, rings, spells, origins, abilities, and classes.
			It initializes these elements from a core XML file and processes the document to populate
			the game's data structures with the specified attributes. This interpreter plays a critical
			role in loading and initializing game data, ensuring that characters and items are created
			with their correct properties and behaviors as defined in the XML.
		]"
	author: "Darvine64"
	date: "April 29th, 18:00"
	revision: "0.2"
	license: "[
	    This file is part of <Eldritch Codex Keeper>, which is distributed under the terms of the
	    GNU General Public License v3.0 or later.
	    
	    <An immersive tool for managing RPG characters, inventories, and adventures, designed for fans of dark fantasy role-playing games.>
	    
	    Copyright (C) <2024>  <Darvine64>

		This program is free software: you can redistribute it and/or modify
	    it under the terms of the GNU General Public License as published by
	    the Free Software Foundation, either version 3 of the License, or
	    (at your option) any later version.

		This program is distributed in the hope that it will be useful,
	    but WITHOUT ANY WARRANTY; without even the implied warranty of
	    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	    GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
	    along with this program. If not, see <http://www.gnu.org/licenses/>.

	 ]"


class
	XML_INTERPRETER
create
	make
feature {NONE}

	make
			-- Initialization for `Current'.
		local
			init_file_name: STRING
			pars: XML_STANDARD_PARSER
			tree_pipe: XML_CALLBACKS_DOCUMENT
		do
			has_error := false
			create classes.make (0)
			create abilities.make (0)
			create origins.make (0)
			create spells.make (0)
			create rings.make (0)
			create weapons.make (0)
			create armors.make (0)
			create consummables.make (0)
			create shields.make (0)
			init_file_name := "./xml/data.xml"
			create pars.make
			create tree_pipe.make_null
			pars.set_callbacks (tree_pipe)
			pars.parse_from_path (create {PATH}.make_from_string (init_file_name))
			if pars.error_occurred then
				has_error := true
				if attached pars.error_message as la_error_message then
					io.error.put_string_32 (la_error_message)
				end
				io.error.flush
			else
				process_document (tree_pipe.document)
			end
		end

feature

	process_document (document: XML_DOCUMENT)
			-- Process `document` to extract and create game elements.
		local
			elements: LIST [XML_ELEMENT]
		do

			if document.root_element.name.is_equal ("data") then
				elements := document.root_element.elements
				from
					elements.start
				until
					elements.off
				loop
					if elements.item_for_iteration.name.is_equal ("consummables") then
						create_consummables (elements.item_for_iteration)
					elseif elements.item_for_iteration.name.is_equal ("armors") then
						create_armors (elements.item_for_iteration)
					elseif elements.item_for_iteration.name.is_equal ("shields") then
						create_shields (elements.item_for_iteration)
					elseif elements.item_for_iteration.name.is_equal ("weapons") then
						create_weapons (elements.item_for_iteration)
					elseif elements.item_for_iteration.name.is_equal ("rings") then
						create_rings (elements.item_for_iteration)
					elseif elements.item_for_iteration.name.is_equal ("spells") then
						create_spells (elements.item_for_iteration)
					elseif elements.item_for_iteration.name.is_equal ("origins") then
						create_origins (elements.item_for_iteration)
					elseif elements.item_for_iteration.name.is_equal ("abilities") then
						create_abilities (elements.item_for_iteration)
					elseif elements.item_for_iteration.name.is_equal ("classes") then
						create_classes (elements.item_for_iteration)
					end
					elements.forth
				end
			end
		end

feature {NONE} -- Helper methods for parsing and creating objects
	parse_integer (a_text: detachable STRING): INTEGER
			-- Parse `a_text` into an integer, returning 0 if `a_text` is void.
		do
			if attached a_text as la_text then
				Result := la_text.to_integer
			else
				Result := 0
			end
		end

	parse_options (a_element: XML_ELEMENT; a_options: LIST [STRING])
			-- Parse options from `element` into `options`.
		local
			l_values_elements: LIST [XML_ELEMENT]
			l_option: STRING
		do
			l_values_elements := a_element.elements
			from
				l_values_elements.start
			until
				l_values_elements.off
			loop
				l_option := l_values_elements.item_for_iteration.text
				if attached l_option as la_option then
					a_options.extend (la_option)
				end
				l_values_elements.forth
			end
		end

	parse_dices (a_element: XML_ELEMENT; a_dices: LIST [DICE])
			-- Parse values from `element`, creates dices and insert them into a dice list
		local
			l_values_elements: LIST [XML_ELEMENT]
			l_value: INTEGER
		do
			l_values_elements := a_element.elements
			from
				l_values_elements.start
			until
				l_values_elements.off
			loop
				l_value := parse_integer (l_values_elements.item_for_iteration.text)
				a_dices.extend (create_dice(l_value))
				l_values_elements.forth
			end
		end

	parse_stats (a_element: XML_ELEMENT; a_stats: LIST [STAT])
			-- Parse values from `element`, creates dices and insert them into a stat list
		local
			l_values_elements: LIST [XML_ELEMENT]
			l_value, l_iterator: INTEGER
			l_stat_names: LIST[STRING]
		do
			create {ARRAYED_LIST [STRING]} l_stat_names.make (0)
			l_stat_names.extend ("Strenght")
			l_stat_names.extend ("Dexterity")
			l_stat_names.extend ("Constitution")
			l_stat_names.extend ("Intelligence")
			l_stat_names.extend ("Wisdom")
			l_stat_names.extend ("Charisma")
			l_iterator := 0
			l_values_elements := a_element.elements
			from
				l_values_elements.start
			until
				l_values_elements.off
			loop
				l_value := parse_integer (l_values_elements.item_for_iteration.text)
				l_iterator := l_iterator + 1
				a_stats.extend (create_stat(l_value,l_iterator, l_stat_names))
				l_values_elements.forth
			end
		end

	parse_abilities (a_element: XML_ELEMENT; a_abilities: LIST [ABILITY])
			-- Parse values from `element`, creates dices and insert them into an ability list
		local
			l_values_elements: LIST [XML_ELEMENT]
			l_value: STRING
			l_ability: ABILITY
			l_found: BOOLEAN
		do
			l_values_elements := a_element.elements
			l_found := false
			from
				l_values_elements.start
			until
				l_values_elements.off
			loop
				l_value := l_values_elements.item_for_iteration.text
				from
					abilities.start
				until
					l_found or abilities.off
				loop
					if attached l_value as la_value then
						if abilities.item_for_iteration.unique_name.is_equal (la_value)  then
							a_abilities.extend (abilities.item_for_iteration)
							l_found := true
						end
					end


					abilities.forth
				end
				l_found := false
				l_values_elements.forth
			end
		end

feature {NONE} -- Creating object list

	create_abilities (a_elements_item: XML_ELEMENT)
			-- Create and add ability items from `a_elements_item` to `abilities`.
		local
			l_ability_elements: LIST [XML_ELEMENT]
		do
			l_ability_elements := a_elements_item.elements
			from
				l_ability_elements.start
			until
				l_ability_elements.off
			loop
				if attached create_ability (l_ability_elements.item) as la_ability then
					abilities.extend (la_ability)
				end
				l_ability_elements.forth
			end
		end

	create_consummables (a_elements_item: XML_ELEMENT)
			-- Create and add consummable items from `elements_item` to `consummables`.
		local
			l_consummable_elements: LIST [XML_ELEMENT]
		do
			l_consummable_elements := a_elements_item.elements
			from
				l_consummable_elements.start
			until
				l_consummable_elements.off
			loop
				if attached create_consummable (l_consummable_elements.item) as la_consummable then
					consummables.extend (la_consummable)
				end
				l_consummable_elements.forth
			end
		end

	create_armors (a_elements_item: XML_ELEMENT)
			-- Create and add armor items from `elements_item` to `armors`.
		local
			l_armor_elements: LIST [XML_ELEMENT]
		do
			l_armor_elements := a_elements_item.elements
			from
				l_armor_elements.start
			until
				l_armor_elements.off
			loop
				if attached create_armor (l_armor_elements.item) as la_armor then
					armors.extend (la_armor)
				end
				l_armor_elements.forth
			end
		end

	create_shields (a_elements_item: XML_ELEMENT)
			-- Create and add shield items from `elements_item` to `shields`.
		local
			l_shield_elements: LIST [XML_ELEMENT]
		do
			l_shield_elements := a_elements_item.elements
			from
				l_shield_elements.start
			until
				l_shield_elements.off
			loop
				if attached create_shield (l_shield_elements.item) as la_shield then
					shields.extend (la_shield)
				end
				l_shield_elements.forth
			end
		end

	create_weapons (a_elements_item: XML_ELEMENT)
			-- Create and add weapon items from `elements_item` to `weapons`.
		local
			l_weapon_elements: LIST [XML_ELEMENT]
		do
			l_weapon_elements := a_elements_item.elements
			from
				l_weapon_elements.start
			until
				l_weapon_elements.off
			loop
				if attached create_weapon (l_weapon_elements.item) as la_weapon then
					weapons.extend (la_weapon)
				end
				l_weapon_elements.forth
			end
		end

	create_rings (a_elements_item: XML_ELEMENT)
			-- Create and add ring items from `elements_item` to `rings`.
		local
			l_ring_elements: LIST [XML_ELEMENT]
		do
			l_ring_elements := a_elements_item.elements
			from
				l_ring_elements.start
			until
				l_ring_elements.off
			loop
				if attached create_ring (l_ring_elements.item) as la_ring then
					rings.extend (la_ring)
				end
				l_ring_elements.forth
			end
		end

	create_spells (a_elements_item: XML_ELEMENT)
			-- Create and add spell items from `elements_item` to `spells`.
		local
			l_spell_elements: LIST [XML_ELEMENT]
		do
			l_spell_elements := a_elements_item.elements
			from
				l_spell_elements.start
			until
				l_spell_elements.off
			loop
				if attached create_spell (l_spell_elements.item) as la_spell then
					spells.extend (la_spell)
				end
				l_spell_elements.forth
			end
		end

	create_origins (a_elements_item: XML_ELEMENT)
			-- Create and add origin items from `elements_item` to `origins`.
		local
			l_origin_elements: LIST [XML_ELEMENT]
		do
			l_origin_elements := a_elements_item.elements
			from
				l_origin_elements.start
			until
				l_origin_elements.off
			loop
				if attached create_origin (l_origin_elements.item) as la_origin then
					origins.extend (la_origin)
				end
				l_origin_elements.forth
			end
		end

	create_classes (a_elements_item: XML_ELEMENT)
			-- Create and add class items from `elements_item` to `classes`.
		local
			l_class_elements: LIST [XML_ELEMENT]
		do
			l_class_elements := a_elements_item.elements
			from
				l_class_elements.start
			until
				l_class_elements.off
			loop
				if attached create_class (l_class_elements.item) as la_class then
					classes.extend (la_class)
				end
				l_class_elements.forth
			end
		end

feature {NONE} -- creating object



	create_stat (a_stat_value, a_iterator: INTEGER a_stat_names: LIST[STRING]): STAT
			-- Create a dice based on his highest value
			-- `a_dice_high_value` is the highest value of the dice that will be created
		do

			create Result.make(a_stat_names.at (a_iterator), a_stat_value)

		end

	create_dice (a_dice_high_value: INTEGER): DICE
			-- Create a dice based on his highest value
			-- `a_dice_high_value` is the highest value of the dice that will be created
		do

			create Result.make(a_dice_high_value)

		end

	create_ability (a_elements_item: XML_ELEMENT): detachable ABILITY
			-- Create an ability object from `a_elements_item`.
			-- `a_elements_item`: XML element representing an ability.
		local
			l_child_elements: LIST [XML_ELEMENT]
			l_name, l_unique_name, l_description: STRING
			l_required_class_level, l_usage: INTEGER
			l_options: LIST [STRING]
		do
			Result := void
			l_name := ""
			l_unique_name := ""
			l_description := ""
			create {ARRAYED_LIST [STRING]} l_options.make (0)
			l_required_class_level := 0
			l_usage := 0
			l_child_elements := a_elements_item.elements
			across
				l_child_elements as la_cursor
			loop
				if la_cursor.item.name.is_equal ("name") then
					l_name := la_cursor.item.text
				elseif la_cursor.item.name.is_equal ("unique_name") then
					l_unique_name := la_cursor.item.text
				elseif la_cursor.item.name.is_equal ("description") then
					l_description := la_cursor.item.text
				elseif la_cursor.item.name.is_equal ("usage") then
					l_usage := parse_integer (la_cursor.item.text)
				elseif la_cursor.item.name.is_equal ("options") then
					parse_options (la_cursor.item, l_options)
				elseif la_cursor.item.name.is_equal ("required_class_level") then
					l_required_class_level := parse_integer (la_cursor.item.text)
				end
			end
			if attached l_name as la_name and attached l_unique_name as la_unique_name and attached l_description as la_description then
				create Result.make (la_name, la_unique_name, la_description, l_usage, l_required_class_level, l_options)
			end
		end

	create_consummable (a_elements_item: XML_ELEMENT): detachable CONSUMMABLE
			-- Create a consummable object from `a_elements_item`.
			-- `a_elements_item`: XML element representing a consumable item.
		local
			l_child_elements: LIST [XML_ELEMENT]
			l_name, l_effect: STRING
			l_cost: INTEGER
		do
			Result := void
			l_name := ""
			l_effect := ""
			l_cost := 0
			l_child_elements := a_elements_item.elements
			across
				l_child_elements as la_cursor
			loop
				if la_cursor.item.name.is_equal ("name") then
					l_name := la_cursor.item.text
				elseif la_cursor.item.name.is_equal ("cost") then
					l_cost := parse_integer (la_cursor.item.text)
				elseif la_cursor.item.name.is_equal ("effect") then
					l_effect := la_cursor.item.text
				end

			end
			if attached l_name as la_name and attached l_effect as la_effect then
				create Result.make (la_name, l_cost, la_effect)
			end
		end

	create_armor (a_elements_item: XML_ELEMENT): detachable ARMOR
			-- Create an armor object from `a_elements_item`.
			-- `a_elements_item`: XML element representing an armor item.
		local
			l_name, l_effect, l_description, l_armor_class_stat_modifier: STRING
			l_cost, l_armor_class, l_strength_requirement: INTEGER
			l_child_elements: LIST [XML_ELEMENT]
		do
			Result := void
			l_name := ""
			l_effect := ""
			l_description := ""
			l_armor_class_stat_modifier := ""
			l_cost := 0
			l_armor_class := 0
			l_strength_requirement := 0
			l_child_elements := a_elements_item.elements
			across
				l_child_elements as la_cursor
			loop
				if la_cursor.item.name.is_equal ("name") then
					l_name := la_cursor.item.text
				elseif la_cursor.item.name.is_equal ("effect") then
					l_effect := la_cursor.item.text
				elseif la_cursor.item.name.is_equal ("description") then
					l_description := la_cursor.item.text
				elseif la_cursor.item.name.is_equal ("cost") then
					l_cost := parse_integer (la_cursor.item.text)
				elseif la_cursor.item.name.is_equal ("armor_class") then
					l_armor_class := parse_integer (la_cursor.item.text)
				elseif la_cursor.item.name.is_equal ("armor_class_stat_modifier") then
					l_armor_class_stat_modifier := la_cursor.item.text
					if attached la_cursor.item.text as la_text then
						l_armor_class_stat_modifier := la_text
					else
						l_armor_class_stat_modifier := ""
					end
				elseif la_cursor.item.name.is_equal ("strength_requirement") then
					l_strength_requirement := parse_integer (la_cursor.item.text)
				end
			end
			if attached l_name as la_name and attached l_effect as la_effect and attached l_description as la_description and attached l_armor_class_stat_modifier as la_armor_class_stat_modifier then
				create Result.make (la_name, la_effect, la_description, la_armor_class_stat_modifier, l_cost, l_armor_class, l_strength_requirement)
			end
		end

	create_shield (a_elements_item: XML_ELEMENT): detachable SHIELD
			-- Create a shield object from `a_elements_item`.
			-- `a_elements_item`: XML element representing a shield.
		local
			l_name, l_effect, l_description, l_type: STRING
			l_cost, l_armor_class, l_strength_requirement: INTEGER
			l_child_elements: LIST [XML_ELEMENT]
		do
			Result := void
			l_name := ""
			l_effect := ""
			l_description := ""
			l_type := ""
			l_cost := 0
			l_armor_class := 0
			l_strength_requirement := 0
			l_child_elements := a_elements_item.elements
			across
				l_child_elements as la_cursor
			loop
				if la_cursor.item.name.is_equal ("name") then
					l_name := la_cursor.item.text
				elseif la_cursor.item.name.is_equal ("effect") then
					l_effect := la_cursor.item.text
				elseif la_cursor.item.name.is_equal ("description") then
					l_description := la_cursor.item.text
				elseif la_cursor.item.name.is_equal ("cost") then
					l_cost := parse_integer (la_cursor.item.text)
				elseif la_cursor.item.name.is_equal ("armor_class") then
					l_armor_class := parse_integer (la_cursor.item.text)
				elseif la_cursor.item.name.is_equal ("type") then
					l_type := la_cursor.item.text
				elseif la_cursor.item.name.is_equal ("strength_requirement") then
					l_strength_requirement := parse_integer (la_cursor.item.text)
				end
			end
			if attached l_name as la_name and attached l_effect as la_effect and attached l_description as la_description and attached l_type as la_type then
				create Result.make (la_name, la_effect, la_description, la_type, l_cost, l_armor_class, l_strength_requirement)
			end
		end

	create_spell (a_elements_item: XML_ELEMENT): detachable SPELL
			-- Create a spell object from `a_elements_item`.
			-- `a_elements_item`: XML element representing a spell.
		local
			l_name, l_effect, l_cast_time_type, l_duration, l_range, l_cost, l_spell_type: STRING
			l_cast_time_number, l_casts, l_level, l_attunement_slots: INTEGER
			l_child_elements: LIST [XML_ELEMENT]
		do
			Result := void
			l_name := ""
			l_effect := ""
			l_cast_time_type := ""
			l_duration := ""
			l_range := ""
			l_cost := ""
			l_spell_type := ""
			l_cast_time_number := 0
			l_casts := 0
			l_level := 0
			l_attunement_slots := 0
			l_child_elements := a_elements_item.elements
			across
				l_child_elements as la_cursor
			loop
				if la_cursor.item.name.is_equal ("name") then
					l_name := la_cursor.item.text
				elseif la_cursor.item.name.is_equal ("effect") then
					l_effect := la_cursor.item.text
				elseif la_cursor.item.name.is_equal ("cast_time_type") then
					l_cast_time_type := la_cursor.item.text
				elseif la_cursor.item.name.is_equal ("duration") then
					l_duration := la_cursor.item.text
				elseif la_cursor.item.name.is_equal ("range") then
					l_range := la_cursor.item.text
				elseif la_cursor.item.name.is_equal ("cost") then
					l_cost := la_cursor.item.text
				elseif la_cursor.item.name.is_equal ("spell_type") then
					l_spell_type := la_cursor.item.text
				elseif la_cursor.item.name.is_equal ("cast_time_number") then
					l_cast_time_number := parse_integer (la_cursor.item.text)
				elseif la_cursor.item.name.is_equal ("casts") then
					l_casts := parse_integer (la_cursor.item.text)
				elseif la_cursor.item.name.is_equal ("level") then
					l_level := parse_integer (la_cursor.item.text)
				elseif la_cursor.item.name.is_equal ("attunement_slots") then
					l_attunement_slots := parse_integer (la_cursor.item.text)
				end
			end
			if attached l_name as la_name and attached l_effect as la_effect and attached l_cast_time_type as la_cast_time_type and attached l_duration as la_duration and attached l_range as la_range and attached l_cost as la_cost and attached l_spell_type as la_spell_type then
				create Result.make (la_name, la_effect, la_cast_time_type, la_duration, la_range, la_cost,la_spell_type, l_cast_time_number, l_casts, l_level, l_attunement_slots, false)
			end
		end

	create_weapon (a_elements_item: XML_ELEMENT): detachable WEAPON
		-- Create a weapon object from `a_elements_item`.
		-- `a_elements_item`: XML element representing a weapon.
		local
			l_name, l_effect, l_type, l_damage_type: STRING
			l_cost, l_strength_requirement, l_number_dice: INTEGER
			l_properties: LIST[STRING]
			l_damage_dices: LIST[DICE]
			l_child_elements: LIST [XML_ELEMENT]
		do
			Result := void
			l_name := ""
			l_effect := ""
			l_type := ""
			l_damage_type := ""
			l_cost := 0
			l_strength_requirement := 0
			l_number_dice := 0
			create {ARRAYED_LIST [STRING]} l_properties.make (0)
			create {ARRAYED_LIST [DICE]} l_damage_dices.make (0)
			l_child_elements := a_elements_item.elements
			across
				l_child_elements as la_cursor
			loop
				if la_cursor.item.name.is_equal ("name") then
					l_name := la_cursor.item.text
				elseif la_cursor.item.name.is_equal ("effect") then
					l_effect := la_cursor.item.text
				elseif la_cursor.item.name.is_equal ("type") then
					l_type := la_cursor.item.text
				elseif la_cursor.item.name.is_equal ("damage_type") then
					l_damage_type := la_cursor.item.text
				elseif la_cursor.item.name.is_equal ("cost") then
					l_cost := parse_integer (la_cursor.item.text)
				elseif la_cursor.item.name.is_equal ("strength_requirement") then
					l_strength_requirement := parse_integer (la_cursor.item.text)
				elseif la_cursor.item.name.is_equal ("number_dice") then
					l_number_dice := parse_integer (la_cursor.item.text)
				elseif la_cursor.item.name.is_equal ("properties") then
					parse_options (la_cursor.item, l_properties)
				elseif la_cursor.item.name.is_equal ("damage_dices") then
					parse_dices (la_cursor.item, l_damage_dices)
				end
			end
			if attached l_name as la_name and attached l_effect as la_effect and attached l_type as la_type and attached l_damage_type as la_damage_type then
				create Result.make (la_name, la_effect, la_type, la_damage_type, l_cost, l_strength_requirement,l_number_dice, l_properties, l_damage_dices)
			end
		end

	create_ring (a_elements_item: XML_ELEMENT): detachable RING
			-- Create a ring object from `a_elements_item`.
			-- `a_elements_item`: XML element representing a consumable item.
		local
			l_child_elements: LIST [XML_ELEMENT]
			l_name, l_effect: STRING
			l_cost: INTEGER
		do
			Result := void
			l_name := ""
			l_effect := ""
			l_cost := 0
			l_child_elements := a_elements_item.elements
			across
				l_child_elements as la_cursor
			loop
				if la_cursor.item.name.is_equal ("name") then
					l_name := la_cursor.item.text
				elseif la_cursor.item.name.is_equal ("cost") then
					l_cost := parse_integer (la_cursor.item.text)
				elseif la_cursor.item.name.is_equal ("effect") then
					l_effect := la_cursor.item.text
				end

			end
			if attached l_name as la_name and attached l_effect as la_effect then
				create Result.make (la_name, l_cost, la_effect)
			end
		end

	create_origin (a_elements_item: XML_ELEMENT): detachable ORIGIN
		-- Create an origin object from `a_elements_item`.
		-- `a_elements_item`: XML element representing an origin.
		local
			l_name, l_description: STRING
			l_speed: INTEGER
			l_bloodied_effects: LIST[STRING]
			l_base_stats: LIST[STAT]
			l_position_dice: DICE
			l_child_elements: LIST [XML_ELEMENT]
		do
			Result := void
			l_name := ""
			l_description := ""
			l_speed := 0
			create {ARRAYED_LIST [STRING]} l_bloodied_effects.make (0)
			create {ARRAYED_LIST [STAT]} l_base_stats.make (0)
			create l_position_dice.make (0)
			l_child_elements := a_elements_item.elements
			across
				l_child_elements as la_cursor
			loop
				if la_cursor.item.name.is_equal ("name") then
					l_name := la_cursor.item.text
				elseif la_cursor.item.name.is_equal ("description") then
					l_description := la_cursor.item.text
				elseif la_cursor.item.name.is_equal ("bloodied_effects") then
					parse_options (la_cursor.item, l_bloodied_effects)
				elseif la_cursor.item.name.is_equal ("speed") then
					l_speed := parse_integer (la_cursor.item.text)
				elseif la_cursor.item.name.is_equal ("base_stats") then
					parse_stats (la_cursor.item, l_base_stats)
				elseif la_cursor.item.name.is_equal ("position_dice") then
					l_position_dice.max_value.set_item (parse_integer (la_cursor.item.text))
				end
			end
			if attached l_name as la_name and attached l_description as la_description then
				create Result.make (la_name, la_description, l_bloodied_effects, l_speed, l_base_stats, l_position_dice)
			end
		end

	create_class (a_elements_item: XML_ELEMENT): detachable CHARACTER_CLASS
		-- Create a character_class object from `a_elements_item`.
		-- `a_elements_item`: XML element representing a character_class.
		local
			l_name, l_description: STRING
			l_level: INTEGER
			l_saving_throw_proficiencies, l_primary_stats, l_skill_proficiencies, l_starting_equipments: LIST[STRING]
			l_abilities: LIST[ABILITY]
			l_child_elements: LIST [XML_ELEMENT]
		do
			Result := void
			l_name := ""
			l_description := ""
			l_level := 0
			create {ARRAYED_LIST [STRING]} l_saving_throw_proficiencies.make (0)
			create {ARRAYED_LIST [STRING]} l_primary_stats.make (0)
			create {ARRAYED_LIST [STRING]} l_skill_proficiencies.make (0)
			create {ARRAYED_LIST [STRING]} l_starting_equipments.make (0)
			create {ARRAYED_LIST [ABILITY]} l_abilities.make (0)
			l_child_elements := a_elements_item.elements
			across
				l_child_elements as la_cursor
			loop
				if la_cursor.item.name.is_equal ("name") then
					l_name := la_cursor.item.text
				elseif la_cursor.item.name.is_equal ("description") then
					l_description := la_cursor.item.text
				elseif la_cursor.item.name.is_equal ("level") then
					l_level := parse_integer (la_cursor.item.text)
				elseif la_cursor.item.name.is_equal ("saving_throw_proficiencies") then
					parse_options (la_cursor.item, l_saving_throw_proficiencies)
				elseif la_cursor.item.name.is_equal ("primary_stats") then
					parse_options (la_cursor.item, l_primary_stats)
				elseif la_cursor.item.name.is_equal ("skill_proficiencies") then
					parse_options (la_cursor.item, l_skill_proficiencies)
				elseif la_cursor.item.name.is_equal ("starting_equipments") then
					parse_options (la_cursor.item, l_starting_equipments)
				elseif la_cursor.item.name.is_equal ("abilities") then
					parse_abilities (la_cursor.item, l_abilities)
				end
			end
			if attached l_name as la_name and attached l_description as la_description then
				create Result.make (la_name, la_description, l_level, l_saving_throw_proficiencies, l_primary_stats, l_skill_proficiencies, l_starting_equipments, l_abilities)
			end
		end

feature

	has_error: BOOLEAN -- Indicates if an error occurred during XML parsing.

		-- Lists to hold game elements created from the XML document.
	consummables: ARRAYED_LIST [CONSUMMABLE]
	armors: ARRAYED_LIST [ARMOR]
	shields: ARRAYED_LIST [SHIELD]
	weapons: ARRAYED_LIST [WEAPON]
	rings: ARRAYED_LIST [RING]
	spells: ARRAYED_LIST [SPELL]
	origins: ARRAYED_LIST [ORIGIN]
	abilities: ARRAYED_LIST [ABILITY]
	classes: ARRAYED_LIST [CHARACTER_CLASS]

end
